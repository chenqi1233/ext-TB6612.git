//% color="#4169E1" iconWidth=50 iconHeight=40
namespace TB6612FNG{
  
    //% block="TB6612FNG Motor Init Board[BOARD]PWM1pin [E1]DIR1pin [M1]PWM2pin [E2]DIR2pin [M2]    " blockType="command"
    //% BOARD.shadow="dropdown" BOARD.options="BOARD" 
    //% E1.shadow="dropdown" E1.options="E1" 
    //% M1.shadow="dropdown" M1.options="M1"
    //% E2.shadow="dropdown" E2.options="E2"   
    //% M2.shadow="dropdown" M2.options="M2"
    export function TB6612FNGInit(parameter: any, block: any) {      
     let e1=parameter.E1.code;  
     let m1=parameter.M1.code;
     let e2=parameter.E2.code;  
     let m2=parameter.M2.code;
     let board=parameter.BOARD.code;

     Generator.addInclude("TB6612FNG","#include <TB6612FNG.h>");
     Generator.addObject(`TB6612FNG${board}` ,`TB6612FNG`,`tb${board}(${e1},${m1},${e2},${m2});`);   
    }
    //% block="TB6612FNG Motor Board [BOARD]Motor [MOTOR]Dir [FR]Speed[SPEED]  " blockType="command"
    //% BOARD.shadow="dropdown" BOARD.options="BOARD"    
    //% MOTOR.shadow="dropdownRound" MOTOR.options="MOTOR" 
    //% FR.shadow="dropdownRound" FR.options="FR"
    //% SPEED.shadow="range" SPEED.params.min="0" SPEED.params.max="255" SPEED.defl="200"
    export function TB6612FNGInit1(parameter: any, block: any) {        
     let board=parameter.BOARD.code;         
     let fr=parameter.FR.code;  
     let speed=parameter.SPEED.code;
     let m=parameter.MOTOR.code;
     
      

      Generator.addCode(`tb${board}.setSpeed(${m},${fr},${speed});`);


        
     }

    //% block="TB6612FNG Board [BOARD]Motor [MOTOR] " blockType="command"
    //% BOARD.shadow="dropdown" BOARD.options="BOARD"    
    //% MOTOR.shadow="dropdownRound" MOTOR.options="MOTOR" 
    
    export function TB6612FNGInit2(parameter: any, block: any) {        
      let board=parameter.BOARD.code;         
      let m=parameter.MOTOR.code;
 
       Generator.addCode(`tb${board}.stop(${m});`);
      
            
           
        }
      
   
    
    
}